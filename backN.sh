#!/bin/bash

N=$1
# verific ca valoarea impusa sa fie valida
if [[ $N -lt 0 || $N -gt 16 ]]; then
    echo "N has to be higher than 0 but lower then 16"
fi 


for i in $( seq  1 $N);
do
    cd ..
done

echo "We're back " $N " folders!"
echo "Now we're at: " $(pwd)
