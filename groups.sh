#!/bin/bash

#Groups and users

os=$(less /etc/group | grep "devops"| cut -c 1-6)
user=$(awk -F: '{ print $1 }' /etc/passwd | grep "engineer")

if [ "$os" == "devops" ]; then
	echo "DevOps group exists!"
else
	sudo groupadd devops
fi

if [ "$user" == "engineer" ]; then
	echo "User engineer already exists!"
else
	sudo useradd engineer
fi

