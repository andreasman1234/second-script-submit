#!/bin/bash

#top 5 biggest files in a folder

if [ "$#" -ne 1 ]; then
    echo "Folderul nu a fost specificat"
    exit
fi

locatie_director=$1

cd $locatie_director
du -hsx * | sort -rh | head -5
du -hsx -- * | sort -rh | head -5
